import unittest
import os
import data_handler
import cryptography
from cryptography.hazmat.primitives.serialization import PublicFormat
from cryptography.hazmat.primitives.serialization import Encoding
import base64


class TestDataHandler(unittest.TestCase):

    def tearDownClass():
        db_host = "localhost"
        db_port = 27017
        db_connection = data_handler.establish_db_connection(db_host, db_port)
        db_connection.collection.drop_collection

    def test_write_csv_file(self):
        file_path = "test_data_01.csv"
        attendee_01 = {'key': '01', 'data': 'Max Mustermann 01.01.1970'}
        attendee_02 = {'key': '02', 'data': 'Monika Muster 01.01.1970'}
        attendees = [attendee_01, attendee_02]

        data_handler.write_csv(file_path, attendee_01)
        data_handler.write_csv(file_path, attendee_02)
        # Check if file is written
        self.assertTrue(os.path.exists(file_path))
        os.remove(file_path)

    def test_read_csv_file(self):
        file_path = "test_data_02.csv"
        attendee_01 = {'key': '01', 'data': 'Max Mustermann 01.01.1970'}
        attendee_02 = {'key': '02', 'data': 'Monika Muster 01.01.1970'}
        data_handler.write_csv(file_path, attendee_01)
        data_handler.write_csv(file_path, attendee_02)

        attendee_data = data_handler.read_csv(file_path)
        self.assertLessEqual(attendee_data[0].items(), attendee_01.items())
        self.assertLessEqual(attendee_data[1].items(), attendee_02.items())
        os.remove(file_path)

    def test_load_pub_key(self):
        file_path = "tests/pub_key.pem"
        pub_key = data_handler.read_public_key(file_path)
        pub_key_pem = pub_key.public_bytes(
            encoding=Encoding.PEM,
            format=PublicFormat.SubjectPublicKeyInfo)

        with open(file_path, mode='rb') as file:
            file_data = file.read()

        self.assertEqual(file_data, pub_key_pem)

    def test_encrypt_with_pub_key(self):
        file_path = "tests/pub_key.pem"
        pub_key = data_handler.read_public_key(file_path)
        priv_key = data_handler.read_private_key("tests/test_not_use_in_production.key")
        msg = "The quick brown fox jumps over the lazy dog"

        encrypted = data_handler.encrypt_input_data(message=msg, public_key=pub_key)
        # print(len(encrypted))

        decrypt = data_handler.decrypt_ciphertext(encrypted, priv_key)
        # print(decrypt)

    def test_write_encrypted_data_to_csv_file(self):
        file_path = "tests/test_data_01.csv"
        pub_key = data_handler.read_public_key("tests/pub_key.pem")
        data = "Max Mustermann 01.01.1970"
        encrypted_data = data_handler.encrypt_input_data(data, pub_key)
        encrypted_data = base64.standard_b64encode(encrypted_data)
        attendee_01 = {'key': '01', 'data': f"{encrypted_data}"}
#        print(attendee_01.items())

        data_handler.write_csv(file_path, attendee_01)
        attendee_data = data_handler.read_csv(file_path)
        # print(attendee_data[0].items())
        self.assertLessEqual(attendee_data[0].items(), attendee_01.items())
        os.remove(file_path)

    # def test_read_encrypted_data_from_csv_file(self):
    #     file_path = "tests/encrypted_data_01.csv"
    #     priv_key = data_handler.read_private_key("tests/test_not_use_in_production.key")

    #     csv_data = data_handler.read_csv(file_path)
    #     encrypted_item_data = csv_data[0].get('data')
    #     print(encrypted_item_data)
    #     encrypted_item_data = base64.standard_b64decode(encrypted_item_data)
    #     print(encrypted_item_data)
    #     print(len(encrypted_item_data))
    #     decrypted_item_data = data_handler.decrypt_ciphertext(encrypted_item_data, priv_key)
    #     # print(decrypted_item_data)

    def test_connect_db(self):
        db_host = "localhost"
        db_port = 27017
        db_connection = data_handler.establish_db_connection(db_host, db_port)

        self.assertIsNotNone(db_connection)

    def test_write_single_entry_into_db(self):
        db_host = "localhost"
        db_port = 27017
        db_connection = data_handler.establish_db_connection(db_host, db_port)

        attendee_01 = {'key': '01', 'data': 'Max Mustermann 01.01.1970'}
        obj_id = data_handler.write_db_entry(db_connection, attendee_01)
        self.assertIsNotNone(obj_id)
        delete_result = data_handler.remove_entry(db_connection, obj_id)

    def test_write_and_read_entry(self):
        db_host = "localhost"
        db_port = 27017
        db_connection = data_handler.establish_db_connection(db_host, db_port)

        attendee_01 = {'key': '01', 'data': 'Max Mustermann 01.01.1970'}
        obj_id = data_handler.write_db_entry(db_connection, attendee_01)
        entry = data_handler.read_db_entry(db_connection, obj_id)
        self.assertEqual(attendee_01['data'], entry['data'])
        self.assertEqual(attendee_01['key'], entry['key'])
        self.assertEqual(obj_id, entry['_id'])
        delete_result = data_handler.remove_entry(db_connection, obj_id)

    def test_write_and_read_encrypted_data(self):
        db_host = "localhost"
        db_port = 27017
        db_connection = data_handler.establish_db_connection(db_host, db_port)

        pub_key = data_handler.read_public_key("tests/pub_key.pem")
        data = "Max Mustermann 01.01.1970"
        encrypted_data = data_handler.encrypt_input_data(data, pub_key)
        encrypted_data = base64.standard_b64encode(encrypted_data)

        attendee_01 = {'key': '01', 'data': f"{encrypted_data}"}
#        print(attendee_01.items())

        obj_id = data_handler.write_db_entry(db_connection, attendee_01)
        entry = data_handler.read_db_entry(db_connection, obj_id)
#        print(entry)
        self.assertEqual(attendee_01['data'], entry['data'])
        self.assertEqual(attendee_01['key'], entry['key'])
        delete_result = data_handler.remove_entry(db_connection, obj_id)

    def test_write_encrypted_data_and_read_to_decrypt(self):
        db_host = "localhost"
        db_port = 27017
        db_connection = data_handler.establish_db_connection(db_host, db_port)

        pub_key = data_handler.read_public_key("tests/pub_key.pem")
        data = "Max Mustermann 01.01.1970"
        encrypted_data = data_handler.encrypt_input_data(data, pub_key)
        encrypted_data = base64.standard_b64encode(encrypted_data)
        attendee_01 = {'key': '01', 'data': encrypted_data}

        obj_id = data_handler.write_db_entry(db_connection, attendee_01)
        entry = data_handler.read_db_entry(db_connection, obj_id)
        self.assertEqual(attendee_01['data'], entry['data'])
        self.assertEqual(attendee_01['key'], entry['key'])
        delete_result = data_handler.remove_entry(db_connection, obj_id)

        priv_key = data_handler.read_private_key("tests/test_not_use_in_production.key")
        read_encrypted_data = entry['data']
        decrypted_data = data_handler.decrypt_ciphertext(base64.b64decode(read_encrypted_data), priv_key).decode('utf-8')
        self.assertEqual(data, decrypted_data)