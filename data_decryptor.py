#!/usr/bin/python

import argparse
import importlib
import logging
import binascii
import base64
import cryptography
import data_handler
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding

# configure logger and set default log level
logging.basicConfig(format='%(asctime)s - %(levelname)s :: %(message)s')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "-l",
        "--loglevel",
        help="Setting the log level.",
        choices=["debug", "info", "warn", "error", "fatal", "critical"],
        default="info")
    parser.add_argument("-e", "--ciphertext", help="Base64 encoded ciphertext input to decrypt", required=True)
    parser.add_argument("-inkey", "--private-key", help="Path to private key file", required=True)
    args = parser.parse_args()
    numeric_lvl = getattr(logging, args.loglevel.upper(), None)
    logging.getLogger().setLevel(level=numeric_lvl)

    priv_key_path = args.private_key
    priv_key = data_handler.read_private_key(priv_key_path)
    logging.info(f"Loaded private key from file: {priv_key_path}")
    encrypted_text = args.ciphertext
    decrypted_text = data_handler.decrypt_ciphertext(base64.b64decode(encrypted_text), priv_key)
    print(decrypted_text.decode('utf-8'))
