#!/usr/bin/env bash

docker stop mongodb
docker rm mongodb
docker pull mongo:2.4.14
docker run -d -p 27017-27019:27017-27019 -p 28017:28017 --name mongodb mongo:2.4.14 --rest