# Registration Service based on QR-Code

## Bootstrapping Raspberry Pi

### Host bootstrapping (Timezone, Users, Security)

* `sudo timedatectl set-timezone UTC`
* `sudo adduser qrregistry`
* `sudo usermod -a -G sudo,plugdev,users,input,netdev,gpio,i2c,spi qrregistry`
* `sudo deluser -remove-home pi`
* `sudo localedef -i de_DE -f UTF-8 de_DE.UTF-8`
* `sudo apt update && apt full-upgrade`
* `sudo apt install ufw`

* `mv /etc/sudoers.d/010_pi-nopasswd /etc/sudoers.d/010_custom`
* `sudo visudo /etc/sudoers.d/010_custom`:

```conf
qrregistry ALL=(ALL) PASSWD: ALL
%sudo ALL=ALL, !/bin/su
```

### NGINX as Reverse Proxy

* `sudo apt install nginx`
* `sudo vi /etc/nginx/nginx.conf`:

```conf
user qrregistry; # run as qrregistry user
```

#### NGINX config to proxy public traffic to service

tbd.

### Install and register MongoDB

* `sudo apt install mongodb`
* `sudo systemctl enable mongodb`
* `sudo systemctl start mongodb`

### Python dependencies

* `sudo apt install python3-pip`
* `sudo pip3 install cherrypy`
* `sudo pip3 install pymongo==3.4.0`

### log to right folder** (not working yet)

* `sudo mkdir /var/log/qrregistry`
* `sudo chgrp qrregistry /var/log/qrregistry`
* `sudo chmod g+w /var/log/qrregistry`

### systemd service registration

* `sudo ln -s /home/qrregistry/qrregistry.service /lib/systemd/system/qrregistry.service`
* `sudo chmod 644 qrregistry.service`
* `sudo systemctl daemon-reload`
* `sudo systemctl enable qrregistry.service`
* `sudo systemctl start qrregistry.service`

## Development Environment

### Assumption

* Python Dependencies installed with `pip3`
* `Docker` installed
* Network traffic to docker containers is possible

### Starting up

1. Start the MongoDB service (either as Docker Container or as reals OS service): `./dev-up.sh`
2. The MongoDB should be available under `localhost:27017` (or the configured values)
3. Start the *qrregistry REST service*: `python3 service.py`

## Deployment

### Service

The scripts and artifacts are copied to the service host

1. Copy files to host: `rsync -v -a . --filter=':- .gitignore' qrregistry@192.168.2.100:`
2. Restart the `qrrregistry.service`: `sudo systemctl restart qrregistry`

The status of the service can be checked with `sudo systemctl status qrregistry`.

### Cryptography

Creating keys:

* **Private Key** `openssl genrsa -out qrregistry_<DATE>.key 4096`
* **Public Key** `openssl rsa -in qrregistry_<DATE>.key -pubout > qrregistry_pub_<DATE>.pem`

**The *Public Key* is used to encrypt the data in the service and is therefore meant to be shared and placed in the service!**

**The *Private Key* is used to decrypt the data and MUST NOT be shared and should be kept on a secure place!**

## Support

### Encrypt data entries

#### Quick & Dirty

Following command will work for testing purpose.

**TODO** Enhance the `data_decryptor.py` to support a JSON input directly.

```bash
while IFS= read -r line; do python3 data_decryptor.py -inkey qrregistry_2020-06-21.key -e $line; done <<(curl -k http://192.168.2.100:8080/scans 2> /dev/null | jq '.[].data."$binary"')
```
