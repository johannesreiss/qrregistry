import csv
import binascii
import base64
import cryptography
import pymongo
from datetime import date
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding
from pymongo import MongoClient
from bson import ObjectId
import logging
import pprint as pp

logging.getLogger().setLevel(level=logging.INFO)
logging.basicConfig(format='%(asctime)s - %(levelname)s :: %(message)s')

HEADER = ['key', 'data']
DELIMITER = ";"


def establish_db_connection(host='127.0.0.1', port=27017, db_name='attendees'):
    logging.info(f"Establish connection to DB host: {host} on port: {port} for database: {db_name}")
    client = MongoClient(host=host, port=port)
    db_connection = client[db_name]
    logging.info(f"Connection to DB established.")
    return db_connection


def write_db_entry(db_connection, entry_data, collection=None):
    logging.info(f"Write given data to database collection")
    if not collection:
        collection = date.today()

    object_id = db_connection.collection.insert_one(entry_data)
    logging.info(f"Given data was written into the database collection and has Object ID: {object_id.inserted_id}")

    return object_id.inserted_id


def read_db_entry(db_connection, obj_id, collection=None):
    logging.info(f"Read entry for given Object ID: {obj_id} from database collection")
    if not collection:
        collection = date.today()

    entry = db_connection.collection.find_one({'_id': ObjectId(obj_id)})
    logging.info(f"Entry for given ObjectID: {obj_id} from database collection read")
    return entry


def list_entries(db_connection, collection=None):
    if not collection:
        collection = date.today()
    entries = db_connection.collection.find()
    return list(entries)


def remove_entry(db_connection, object_id, collection=None):
    if not object_id:
        return None

    if not collection:
        collection = date.today()

    delete_count = db_connection.collection.delete_one({"_id": ObjectId(object_id)})
    return delete_count.deleted_count


def write_csv(file_path, attendee_data):
    with open(file_path, mode='a') as attendee_file:
        attendee_writer = csv.DictWriter(attendee_file,
                                         delimiter=DELIMITER,
                                         dialect=csv.unix_dialect,
                                         fieldnames=HEADER)

        attendee_writer.writerow(attendee_data)


def read_csv(file_path):
    with open(file_path, mode='r') as attendee_file:
        csv_reader = csv.DictReader(attendee_file,
                                    delimiter=DELIMITER,
                                    dialect=csv.unix_dialect,
                                    fieldnames=HEADER)

        return list(csv_reader)


def read_public_key(file_path):
    logging.info(f"Will read the public key from the given file: {file_path}")
    with open(file_path, mode='rb') as pub_key_file:
        pub_key = serialization.load_pem_public_key(
            pub_key_file.read(), backend=default_backend()
        )
    logging.info(f"Public key was read from the given file: {file_path}")

    return pub_key


# Private key is not required on service side
def read_private_key(file_path):
    logging.info(f"Will read the private key from the given file: {file_path}")
    with open(file_path, mode='rb') as priv_key_file:
        priv_key = serialization.load_pem_private_key(
            priv_key_file.read(), backend=default_backend(), password=None
        )
    logging.info(f"Private key was read from the given file: {file_path}")
    return priv_key


def encrypt_input_data(message, public_key):
    logging.info(f"Encrypt the given message with the public_key")
    byte_msg = message.encode('utf-8')
    encrypted_message = public_key.encrypt(
        byte_msg,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    logging.info(f"Given message encrypted with the public key")

    return encrypted_message


def decrypt_ciphertext(ciphertext, private_key):
    logging.info(f"Decrypt the given ciphertext with the private key")
    decrypted_msg = private_key.decrypt(
        ciphertext,
        padding.OAEP(
            mgf=padding.MGF1(algorithm=hashes.SHA256()),
            algorithm=hashes.SHA256(),
            label=None
        )
    )
    logging.info(f"Given ciphertext decrypted with the private key")
    return decrypted_msg
