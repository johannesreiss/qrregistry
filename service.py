#!/usr/bin/python
# -*- coding: utf-8 -*-
import cherrypy
import urllib
import data_handler
import uuid
import json
import datetime;
from bson.json_util import dumps

scans = []


class Scans:
    exposed = True

    def GET(self):
        # scan_data = data_handler.read_csv("data.csv")
        scan_data = data_handler.list_entries(db_conn)
        json_scan_data = dumps(scan_data)
        cherrypy.response.headers['Content-Type'] = "application/json"
        return (json_scan_data.encode('utf-8'))

    def POST(self, **kwargs):
        # cherrypy.response.status = 404
        # cherrypy.response.headers['Custom-Title'] = urllib.parse.quote_plus('My custom error')
        # cherrypy.response.headers['Custom-Message'] = urllib.parse.quote_plus('The record already exists.')
        # in ms, if not set automatic time depending on the length, 0 = forever
        cherrypy.response.headers['Custom-Time'] = '5000'

        content = "unknown content"
        format = "unknown format"

        if "content" in kwargs:
            content = kwargs["content"]
        if "format" in kwargs:
            format = kwargs["format"]

        # scans.append((content, format))
        # data_handler.write_csv("data.csv", {"key": uuid.uuid4(), "data": content})
        encrypted_content = data_handler.encrypt_input_data(content, pub_key)
        content = {'data': encrypted_content, 'time': datetime.datetime.utcnow(), 'encrypted': True}
        id = data_handler.write_db_entry(db_conn, content)
        return ({'object_id': id, 'content': content})
        # return ('Append new scan with content: %s, format %s' % (content, format))


class Summary:
    exposed = True

    def GET(self):
        # scan_data = data_handler.read_csv("data.csv")
        scan_data = data_handler.list_entries(db_conn)
        sum_entries = len(scan_data)
        return ('Sum of scanned entries: %s' % sum_entries)


if __name__ == '__main__':
    db_conn = data_handler.establish_db_connection(host='localhost', port=27017)
    pub_key = data_handler.read_public_key("/home/qrregistry/qrregistry_pub_2020-06-21.pem")

    conf = {'global':
            {'server.socket_host': '0.0.0.0',
             'server.socket_port': 8080},
            '/': {'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
                  'tools.encode.on': True,
                  'tools.encode.encoding': 'utf-8'
                  }
            }

    cherrypy.tree.mount(Summary(), '/summary/', conf)
    cherrypy.quickstart(Scans(), '/scans/', conf)
