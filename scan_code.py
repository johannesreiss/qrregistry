import cv2
import hashlib


def video_reader():
    cam = cv2.VideoCapture(0)
    detector = cv2.QRCodeDetector()
    while True:
        _, img = cam.read()
        data, bbox, _ = detector.detectAndDecode(img)
        if data:
            encoded_data = str.encode(data).strip()
            hashed_data = hashlib.md5(encoded_data)
            print("QR Code detected-->", data, " md5 Hash: ", hashed_data.hexdigest())
        cv2.imshow("img", img)
        if cv2.waitKey(1) == ord("Q"):
            break
    cam.release()
    cv2.destroyAllWindows()


video_reader()
